﻿using System;
using middle_age_story.Characters;
using middle_age_story.GameWorld;
using middle_age_story.Items;

namespace middle_age_story {
	/// <summary>
	/// Game class is the game itself it runs and initializes the game
	/// </summary>
	public class Game {
		private static Game _instance;

		private IInputOutput _inputOutputManger;

		private Player _player;
		private GenderPicker _genderPicker;
		private World _world;
		private CharacterFactory _characterFactory;
		private Inventory _inventory;

		/// <summary>
		/// Private constructor for singleton design pattern (there should only be one Game)
		/// </summary>
		private Game() {
		}

		/// <summary>
		/// Instance makes sure that there is always just one Instance of game
		/// If there is no game, instance will create one and return that
		/// else the current game instance will be returned.
		/// </summary>
		public static Game Instance {
			get {
				if (_instance == null) {
					_instance = new Game();
				}

				return _instance;
			}
		}

		/// <summary>
		/// Start the game
		/// </summary>
		/// <param name="inputOutputManager">This is the console to output to the user</param>
		/// <param name="tadaJson"></param>
		public void Start(IInputOutput inputOutputManager, string jsonFile) {
			// Welcome player
			_inputOutputManger = inputOutputManager;
			_inputOutputManger.PrintWelcome(
				"Welkom bij middle age story!");

			_inputOutputManger.PrintDescription(new string[] {"Eerst moeten we een character voor u aanmaken"}, false);

			// Make a World
			WorldBuilder worldBuilder = new WorldBuilder();
			_world = worldBuilder.Createworld(jsonFile);

			// Create genderpicker
			_genderPicker = GenderPicker.Instance;

			// Create weaponFactory
			_characterFactory = CharacterFactory.Instance;

			// Create a player character
			Createplayer();

			// Start the game
			Gameloop();
		}

		/// <summary>
		/// Creates the player with the information given by player.
		/// </summary>
		private void Createplayer() {
			// Player name
			string playername = _inputOutputManger.GetInput(new string[] {"Wat is uw naam?"}, false);

			// Player gender
			Gender playergender = _genderPicker.GetGenderFromPlayerInput(_inputOutputManger);

			// Player Character 
			Character character =
				CharacterPicker.CreaterCharacterFromPlayerInput(_inputOutputManger, _characterFactory);

			// Create 
			_player = new Player(playername, playergender, character);
			_inventory = new Inventory(_player);
		}

		/// <summary>
		/// Runs the game itself
		/// </summary>
		private void Gameloop() {
			// While true, the game is running.
			while (true) {
				try {
					if (_player.GetCharacter().Health < 0) {
						// Let the player you that he has not completed the game
						_inputOutputManger.PrintDescription(new string[] {"GAME OVER"}, true);
						// Return out of this while loop
						return;
					}

					if (_world.CurrentStep != null) {
						// Check if the game has reached its ending
						if (_world.CurrentStep.SpaceType == SpaceType.End) {
							// Let the player know its the end
							_inputOutputManger.PrintDescription(new string[] {"EINDE"}, true);
							// Return out of this while loop
							return;
						}

						_inputOutputManger.ClearConsole();
						bool invalidInput = false;
						string action = string.Empty;
						while (string.IsNullOrEmpty(action)) {
							// Print current status
							_player.GetCharacter().CurrentStatus(_inputOutputManger);

							// ShowPosibleMovement
							_world.ShowPosibleMovement(_inputOutputManger, _player);

							if (invalidInput) {
								_inputOutputManger.Print(new string[] {"Ongeldige invoer"}, false);
							}

							// Get the player's action
							action = _inputOutputManger.GetInput(
								new string[] {"Wat wil je doen?", "(B)ewegen", "(I)tems"}, false);
							switch (action.ToLower()) {
								case "b":
								case "bewegen":
									// Do movement
									_world.HandleMovement(_inputOutputManger, _player);
									break;
								case "i":
								case "items":
									_inventory.UseInventory(_inputOutputManger);
									break;
								default:
									_inputOutputManger.ClearConsole();
									invalidInput = true;
									action = string.Empty;
									break;
							}
						}
					}
					// stops the game when steps is null (eg. player has no health left)
					else {
						return;
					}
				}
				catch {
					// Crashes and bugs.
					_inputOutputManger.PrintDescription(
						new string[] {"Het lijkt erop dat het spel niet meer reageert, de game is daarom afgesloten."},
						true);
				}
			}
		}
	}
}