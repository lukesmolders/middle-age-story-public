﻿using System;
using System.IO;
using middle_age_story.Properties;

namespace middle_age_story {
	internal static class Program {
		private const string JSONFILE = "final-world.json";

		/// <summary>
		/// Program
		/// </summary>
		/// <param name="args">the json file can be passed thru args</param>
		private static void Main(string[] args) {
			string loadFile = JSONFILE;

			if (args.Length > 0) {
				if (File.Exists(args[0])) {
					loadFile = args[0];
				}
			}
			// Start game with json file and InputOutputManager
			Game.Instance.Start(new InputOutputManager(), loadFile);
		}
	}
}