using middle_age_story.Characters;

namespace middle_age_story.Items {
	/// <summary>
	/// Inventory for player
	/// </summary>
	public class Inventory {
		private Player _player;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="player"></param>
		public Inventory(Player player) {
			_player = player;
		}

		/// <summary>
		/// User uses the inventory
		/// </summary>
		/// <param name="inputOutput">This is the console to output to the user</param>
		public void UseInventory(IInputOutput inputOutput) {
			// Print the items in the inventory
			inputOutput.Print(
				new string[] {$"U heeft {_player.Items.Count} items.", "Uw inventory ziet er als volgt uit:"}, true);

			// index for knowing which item
			int index = 1;
			foreach (Item item in _player.Items) {
				inputOutput.Print(
					new string[] {$"{index++}: {item.Type.ToString()} die {item.ReturningHealth} hartjes herstelt."},
					false);
			}

			// Feedback to player what its options are
			string input = inputOutput.GetInput(new string[] {"Welk item wilt u gebruiken (0 voor geen)"}, false);

			// Parse input from player to int
			if (int.TryParse(input, out int selectedInput)) {
				// Check if the input is lower then index and selected is higher then 0 because humans count from 1 and not 0
				if (selectedInput < index && selectedInput > 0) {
					// Use the item
					_player.UseItem(_player.Items[selectedInput - 1]);

					// Feedback to player that the item is used and what its total amount of health is.
					inputOutput.GetInput(
						new string[] {
							"",
							"U heeft het item gebruikt",
							$"U heeft nu {_player.GetCharacter().Health} hartjes. ENTER"
						},
						true);
					return;
				}
			}
		}
	}
}