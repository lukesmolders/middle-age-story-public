﻿namespace middle_age_story.Items {
	/// <summary>
	/// Itembuilder builds items
	/// </summary>
	public class ItemBuilder {
		private ItemType _type = DEFAULT_ITEM_NAME;
		private int _health = DEFAULT_HEALTH;

		private const int DEFAULT_HEALTH = 10;
		private const ItemType DEFAULT_ITEM_NAME = ItemType.apple;

		/// <summary>
		/// Set the name of the Item instance
		/// </summary>
		/// <param name="itemType">Fruit name</param>
		public ItemBuilder SetType(ItemType itemType) {
			_type = itemType;
			return this;
		}

		/// <summary>
		/// Set the amount of health the item gives to the player when used
		/// </summary>
		/// <param name="returningHealth">Amount of returning health</param>
		public ItemBuilder SetReturningHealth(int returningHealth) {
			_health = returningHealth;
			return this;
		}

		/// <summary>
		/// Return the item
		/// </summary>
		/// <returns>Item</returns>
		public Item GetItem() {
			Item build = new Item(_type, _health);
			return build;
		}

		/// <summary>
		/// Clear the previous build configuration back to default
		/// </summary>
		/// <returns></returns>
		public ItemBuilder ClearBuilder() {
			_type = DEFAULT_ITEM_NAME;
			_health = DEFAULT_HEALTH;
			return this;
		}
	}
}