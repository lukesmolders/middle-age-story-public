﻿namespace middle_age_story.Items {
	/// <summary>
	/// Item that can be used by player and can be found in a Event.
	/// </summary>
	public class Item {
		/// <summary>
		/// Initializes a new instance of the Item class
		/// </summary>
		/// <param name="type">The itemtype</param>
		/// <param name="returningHealth">The amount of health it will return to the player</param>
		public Item(ItemType type, int returningHealth) {
			Type = type;
			ReturningHealth = returningHealth;
		}

		public ItemType Type { get; set; }

		/// <summary>
		/// Every Item has its own characteristics , not every item returns the same amount of health.
		/// </summary>
		public int ReturningHealth { get; set; }
	}

	/// <summary>
	/// Enum with a list of all types of food.
	/// </summary>
	public enum ItemType {
		dubiousFood,
		apple,
		roastedBass,
		SpicyFruit,
		MushroomMix
	}
}