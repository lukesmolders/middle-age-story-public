﻿using System.Collections.Generic;

namespace middle_age_story {
	/// <summary>
	/// IInputOutput interface is a interface for interacting with the console
	/// This is needed in order to test certain functions
	/// </summary>
	public interface IInputOutput {
		/// <summary>
		/// Print text to screen
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		void Print(string[] lines, bool clear);

		/// <summary>
		/// Get input from player
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		/// <returns>string with input from player</returns>
		string GetInput(string[] lines, bool clear);

		/// <summary>
		/// Print a welcome to the player
		/// </summary>
		/// <param name="message">Extra message</param>
		void PrintWelcome(string message);

		/// <summary>
		/// Clear the console
		/// </summary>
		void ClearConsole();

		/// <summary>
		/// Prints the text in destinct way
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		void PrintDescription(string[] lines, bool clear);
	}
}