﻿using middle_age_story.Enemies;
using middle_age_story.Items;

namespace middle_age_story.Events {
	/// <summary>
	/// Event can be attached to a step and will have a enemy encounter + Item to collect
	/// </summary>
	public class Event {
		public Enemy Enemy { get; private set; }

		public Item Item { get; private set; }

		/// <summary>
		/// Initializes a new instance of the Event class
		/// </summary>
		/// <param name="enemy">The Enemy</param>
		/// <param name="item">The Item</param>
		public Event(Enemy enemy, Item item) {
			Enemy = enemy;
			Item = item;
		}
	}
}