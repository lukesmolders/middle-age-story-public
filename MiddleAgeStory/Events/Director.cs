﻿using System;
using middle_age_story.Enemies;
using middle_age_story.Items;

namespace middle_age_story.Events {
	/// <summary>
	/// Director creates new objects
	/// </summary>
	public class Director {
		private readonly Random _random = new Random();

		private const int HEALTHMIN = 4;
		private const int HEALTHMAX = 20;

		private const int DAMAGEMIN = 1;
		private const int DAMAGEMAX = 4;

		private const int HEALMIN = 0;
		private const int HEALMAX = 50;

		/// <summary>
		/// Director function to construct and return a new Enemy object.
		/// </summary>
		/// <param name="enemyBuilder">Builder to use in order to create a new Enemy</param>
		/// <returns>Return the new Enemy object</returns>
		public Enemy ConstructRandomEnemy(EnemyBuilder enemyBuilder) {
			enemyBuilder.SetHealth(_random.Next(HEALTHMIN, HEALTHMAX));
			enemyBuilder.SetAttackDamage(_random.Next(DAMAGEMIN, DAMAGEMAX));

			return enemyBuilder.GetEnemy();
		}

		/// <summary>
		/// Director function to construct and return a new random Item object.
		/// </summary>
		/// <param name="itemBuilder">Builder to use in order to create a new Enemy</param>
		/// <returns>Return the new Item object</returns>
		public Item ConstructRandomItem(ItemBuilder itemBuilder) {
			ItemType randomItem = (ItemType) _random.Next(Enum.GetNames(typeof(ItemType)).Length);
			itemBuilder.SetType(randomItem);

			itemBuilder.SetReturningHealth(_random.Next(HEALMIN, HEALMAX));

			return itemBuilder.GetItem();
		}

		/// <summary>
		/// Director function to construct and return a new Item object.
		/// </summary>
		/// <param name="itemBuilder">Builder to use in order to create a new Enemy</param>
		/// <param name="health">Health that the player will get back from consuming the item</param>
		/// <param name="itemType">Name of the item</param>
		/// <returns></returns>
		public Item ConstructItem(ItemBuilder itemBuilder, int health, ItemType itemType) {
			itemBuilder.SetType(itemType);
			itemBuilder.SetReturningHealth(health);

			return itemBuilder.GetItem();
		}

		/// <summary>
		/// Director function to construct and return a new Enemy object.
		/// </summary>
		/// <param name="enemyBuilder">Builder to use in order to create a new Enemy</param>
		/// <param name="health">Amount of health for the enemy</param>
		/// <param name="attackDamage">The strength of the enemy</param>
		/// <returns></returns>
		public Enemy ConstructEnemy(EnemyBuilder enemyBuilder, int health, int attackDamage) {
			enemyBuilder.SetHealth(health);
			enemyBuilder.SetAttackDamage(attackDamage);

			return enemyBuilder.GetEnemy();
		}
	}
}