﻿using middle_age_story.Enemies;

namespace middle_age_story.Events {
	/// <summary>
	/// BuildRandomEvent a new Enemy instance
	/// </summary>
	public class EnemyBuilder {
		private string _name;
		private int? _health;
		private int? _damage;

		private const int DEFAULT_HEALTH = 10;
		private const int DEFAULT_DAMAGE = 4;

		/// <summary>
		/// Set the health of the Enemy instance
		/// </summary>
		/// <param name="health">Amount of health for the Enemy instance</param>
		public EnemyBuilder SetHealth(int health) {
			_health = (health);
			return this;
		}

		/// <summary>
		/// Set the attackdamage of the Enemy instance
		/// </summary>
		/// <param name="damage">Amount of attackdamage for the Enemy instance</param>
		public EnemyBuilder SetAttackDamage(int damage) {
			_damage = (damage);
			return this;
		}

		/// <summary>
		/// Get the enemy instance
		/// </summary>
		/// <returns>Enemy</returns>
		public Enemy GetEnemy() {
			Enemy build = new Enemy(_name ?? "Oger");
			build.SetAttackDamage(_damage ?? DEFAULT_DAMAGE);
			build.SetHealth(_health ?? DEFAULT_HEALTH);
			return build;
		}

		/// <summary>
		/// Clear the previous build configurtion back to default
		/// </summary>
		/// <returns></returns>
		public EnemyBuilder ClearBuilder() {
			_name = null;
			_damage = null;
			_health = null;
			return this;
		}
	}
}