﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using middle_age_story.Characters;
using middle_age_story.Events;
using middle_age_story.Models;
using Newtonsoft.Json;

namespace middle_age_story.GameWorld {
	/// <summary>
	/// The game world
	/// </summary>
	public class World {
		// The current location on the map
		public Step CurrentStep { get; set; }

		// This is the gameworld itself in 2d array format
		public Step[,] World2darray { get; set; }


		/// <summary>
		/// Look if the requested step is available and give it back.
		/// </summary>
		/// <param name="y">Y Scale</param>
		/// <param name="x">X Scale</param>
		/// <returns></returns>
		public Step GetStepByCords(int x, int y) {
			// Check if no out of bound positions are being selected
			if (x >= 0 && y >= 0 && y < World2darray.GetLength(0) && x < World2darray.GetLength(1)) {
				// Return step on this position or return null
				return World2darray[MapYtoArrayY(y), x];
			}

			// Else return null
			return null;
		}

		/// <summary>
		/// ShowPosibleMovement when the player wants to visit this step.
		/// </summary>
		/// <param name="inputOutputManager">This is the console to output to the user</param>
		/// <param name="player">The character the player has</param>
		/// <returns>The choosen Direction</returns>
		public void ShowPosibleMovement(IInputOutput inputOutputManager, Player player) {
			// Feedback to player were he is now
			inputOutputManager.PrintDescription(
				new string[] {
					$"{player.GetName()},",
					$"u bent nu bij: {CurrentStep.Title} , {CurrentStep.Description}"
				}, false);

			// Feedback to player with all the descriptions
			inputOutputManager.PrintDescription(GetDirectionTexts(GetPossibleSteps()), false);
		}

		public void HandleMovement(IInputOutput inputOutputManager, Player player) {
			// Next function to see were we are going to
			Step nextStep = WhereTo(inputOutputManager, GetPossibleSteps(), player);
			// Move to the new direction.
			MoveToNextStep(nextStep, inputOutputManager, player);
		}

		/// <summary>
		/// Create a dictionary of all posible directions to move to.
		/// </summary>
		/// <returns>dictonary with all posible steps</returns>
		private Dictionary<string, Step> GetPossibleSteps() {
			// Create a new Dictionary with possible next steps.
			Dictionary<string, Step> possiblestep = new Dictionary<string, Step>();

			// Check if we can go to the lefts
			if (GetStepByCords(CurrentStep.X - 1, CurrentStep.Y)?.AccessFrom["right"] ?? false) {
				possiblestep.Add("links", GetStepByCords(CurrentStep.X - 1, CurrentStep.Y));
			}

			// Check if we can go to the right
			if (GetStepByCords(CurrentStep.X + 1, CurrentStep.Y)?.AccessFrom["left"] ?? false) {
				possiblestep.Add("rechts", GetStepByCords(CurrentStep.X + 1, CurrentStep.Y));
			}

			// Check if we can go to the top
			if (GetStepByCords(CurrentStep.X, CurrentStep.Y - 1)?.AccessFrom["top"] ?? false) {
				possiblestep.Add("onder", GetStepByCords(CurrentStep.X, CurrentStep.Y - 1));
			}

			// Check if we can go to the bottom
			if (GetStepByCords(CurrentStep.X, CurrentStep.Y + 1)?.AccessFrom["bottom"] ?? false) {
				possiblestep.Add("boven", GetStepByCords(CurrentStep.X, CurrentStep.Y + 1));
			}

			return possiblestep;
		}

		/// <summary>
		/// Get nice formated text to print based on possible steps
		/// </summary>
		/// <param name="possiblesteps">postible steps</param>
		/// <returns>string array with lines to print</returns>
		private string[] GetDirectionTexts(Dictionary<string, Step> possiblesteps) {
			// List with all possible directions
			List<string> directionText = new List<string>();

			// Print all possibilites to the console
			int printedDirections = 0;
			foreach (KeyValuePair<string, Step> direction in possiblesteps) {
				directionText.Add(
					$"{direction.Key} ziet u een {direction.Value.Title} , {direction.Value.Description}");
				if (printedDirections++ < possiblesteps.Count - 1) {
					directionText.Add("--------------");
				}
			}

			return directionText.ToArray();
		}


		/// <summary>
		/// WhereTo asks the player which direction he wants to take
		/// </summary>
		/// <param name="inputOutputManager">This is the console to output to the user</param>
		/// <param name="possiblestep">directions the player can go to</param>
		/// <param name="player">The character the player has</param>
		/// <param name="currentStep">current step were the player is at this moment</param>
		/// <returns>Step to move to</returns>
		private Step WhereTo(IInputOutput inputOutputManager, Dictionary<string, Step> possiblestep, Player player) {
			// Empty Step with the new direction
			Step newdirection = null;
			// Bool that knows if the next step has been found
			bool foundNextStep = false;
			// Lambda expression to take all keys from _directions and place them in one string with ',' seperator
			if (possiblestep.Count > 0) {
				string allPosibleDirections =
					// Aggregate loops thru all values but remembers what has already been added together.
					possiblestep.Keys.Aggregate((current, possibleDirection) => current + ", " + possibleDirection);

				do {
					// string with he input from player
					string inputFromPlayer = inputOutputManager.GetInput(
						new string[] {$"Welke kant wilt u op? {allPosibleDirections}"},
						false).ToLower();

					// Check if the direction the player wants to go, is allowed
					if (possiblestep.ContainsKey(inputFromPlayer)) {
						newdirection = possiblestep[inputFromPlayer];
						foundNextStep = true;
					}

					// Else feedback to player that the input can not be matched to a direction
					else {
						inputOutputManager.Print(
							new string[] {
								"Dat is geen richting die u op kunt, probeer het nog een keer"
							},
							false);
					}

					// Keep on going with the loop while the player input does not match any of directions
				} while (foundNextStep != true);
			}

			return newdirection;
		}


		/// <summary>
		/// Move to the direction and start the event.
		/// </summary>
		/// <param name="nextStep">next step to move to</param>
		/// <param name="inputOutputManager"></param>
		/// <param name="character"></param>
		private void MoveToNextStep(Step nextStep, IInputOutput inputOutputManager, Player player) {
			// Set the new direction as currentStep
			CurrentStep = nextStep;

			// Event handling
			if (nextStep != null && nextStep.ThisEvent != null) {
				// Feedback to player that there is a enemy
				inputOutputManager.PrintDescription(
					new string[] {
						"Onee!!",
						$"er staat ineens een {nextStep.ThisEvent.Enemy.GetName()} recht tegenover u",
						$"Hij heeft {nextStep.ThisEvent.Enemy.Health} hart punten"
					},
					true);

				// Let the step handle its event.
				HandleEvent(inputOutputManager, player, nextStep);
			}

			// If the player is dead, reset the currentstep.
			if (player.GetCharacter().Health <= 0) {
				CurrentStep = null;
			}
		}

		/// <summary>
		/// Converter for 2dmapArray
		/// </summary>
		/// <param name="y">Y scale</param>
		/// <returns></returns>
		public int MapYtoArrayY(int y) {
			return (World2darray.GetLength(0) - 1) - y;
		}

		/// <summary>
		/// Handles the event 
		/// </summary>
		/// <param name="inputOutputManager">This is the console to output to the user</param>
		/// <param name="player">Character of the player</param>
		/// <param name="newdirection">the new direction the player is going to</param>
		public void HandleEvent(IInputOutput inputOutputManager, Player player, Step newdirection) {
			// If there is no event, return
			if (newdirection.ThisEvent == null) return;

			// start enemy attack.
			newdirection.ThisEvent.Enemy.AttackAware();

			Character character = player.GetCharacter();

			// while enemy and character have enough health, continue with the loop
			while (newdirection.ThisEvent.Enemy.Health > 0 && character.Health > 0) {
				// Enemy attacks the player
				newdirection.ThisEvent.Enemy.Attack(character, inputOutputManager);

				// Player attacks the enemy back.
				character.AttackMove(inputOutputManager, newdirection.ThisEvent.Enemy);

				// Feedback to player how much health there is left.
				inputOutputManager.Print(
					new string[] {
						$"jij hebt nog: {character.Health},",
						$"tegenstander nog {newdirection.ThisEvent.Enemy.Health}"
					},
					false);

				// Short interval to make it look like there is a real fight happening
				GameMomentum.Delay(2000);
			}

			// If player has won, give that feedback to player
			if (character.Health > 0) {
				// Feedback to player that the enemy has been defeated
				inputOutputManager.GetInput(
					new string[] {
						"",
						"De tegenstander is verslagen!",
						"Hopelijk bent u van de schrik verlost en kunnen we weer verder. ENTER"
					},
					true);

				// If there is no event, return
				if (newdirection.ThisEvent.Item == null) return;
				// Add the found item from event to the inventory
				player.AddItem(newdirection.ThisEvent.Item);

				// Feedback to player that a item had been found
				inputOutputManager.GetInput(
					new string[] {
						"",
						"Na goed rondkijken hebt u iets gevonden!",
						$"Je hebt een {newdirection.ThisEvent.Item.Type.ToString()} opgepakt. ENTER"
					},
					true);

				// Set event to null because it has been handled with.
				newdirection.ThisEvent = null;
			}
			// If player has no health left, give that feedback to playr
			else {
				inputOutputManager.GetInput(
					new string[] {
						"",
						$"Onee!",
						"Helaas heeft u het niet overleeft!. ENTER"
					},
					true);
			}
		}
	}
}