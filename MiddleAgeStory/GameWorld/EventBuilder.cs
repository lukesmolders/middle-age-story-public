﻿using middle_age_story.Enemies;
using middle_age_story.Events;
using middle_age_story.Items;

namespace middle_age_story.GameWorld {
	/// <summary>
	/// Eventbuilder builds events to be inserted in world steps.
	/// </summary>
	public class EventBuilder {
		private EnemyBuilder _enemyBuilder;
		private ItemBuilder _itemBuilder;
		private Director _director;


		/// <summary>
		/// Default constructor;
		/// Make director and builders.
		/// </summary>
		public EventBuilder() {
			_enemyBuilder = new EnemyBuilder();
			_itemBuilder = new ItemBuilder();
			_director = new Director();
		}

		/// <summary>
		/// BuildRandomEvent new event
		/// </summary>
		/// <returns>return new event</returns>
		public Event BuildRandomEvent() {
			Enemy enemy = _director.ConstructRandomEnemy(_enemyBuilder);
			Item item = _director.ConstructRandomItem(_itemBuilder);
			ClearBuilders();
			return new Event(enemy, item);
		}

		/// <summary>
		/// Build new event
		/// </summary>
		/// <returns>return new event</returns>
		public Event BuildEvent(int enemyHealth, int enemyDamage, int itemHealth, ItemType itemType) {
			Enemy enemy = _director.ConstructEnemy(_enemyBuilder, enemyHealth, enemyDamage);
			Item item = _director.ConstructItem(_itemBuilder, itemHealth, itemType);
			ClearBuilders();
			return new Event(enemy, item);
		}

		/// <summary>
		/// Clears the builders used in eventbuilder.
		/// </summary>
		private void ClearBuilders() {
			_enemyBuilder.ClearBuilder();
			_itemBuilder.ClearBuilder();
		}
	}
}