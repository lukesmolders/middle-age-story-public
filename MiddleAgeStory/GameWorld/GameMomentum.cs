namespace middle_age_story.GameWorld {
	/// <summary>
	/// GameMomentum handles delay
	/// This will make it look like the game is more imerse
	/// </summary>
	public static class GameMomentum {
		/// <summary>
		/// Delays the game
		/// </summary>
		/// <param name="delayTime">Time in millisecond</param>
		public static void Delay(int delayTime) {
			System.Threading.Thread.Sleep(delayTime);
		}
	}
}