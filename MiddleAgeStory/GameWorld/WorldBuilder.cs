using System;
using System.Collections.Generic;
using System.IO;
using middle_age_story.Models;
using Newtonsoft.Json;

namespace middle_age_story.GameWorld {
	/// <summary>
	/// WorldBuilder builds the world.
	/// </summary>
	public class WorldBuilder {
		private readonly EventBuilder _eventBuilder;

		public WorldBuilder() {
			_eventBuilder = new EventBuilder();
		}

		/// <summary>
		/// Create the world, with a json file in the root directory of the project
		/// </summary>
		/// <param name="jsonFile">The JsonFile to create the world from</param>
		/// <returns></returns>
		public World Createworld(string jsonFile) {
			World world = new World();

			// Get JSON file
			string jsonString = File.ReadAllText(jsonFile);

			// Use NewTonSoft to deserialize the json to the Models.
			WorldModel worldModel = JsonConvert.DeserializeObject<WorldModel>(jsonString);

			// X variable for the 2d array
			int maxX = 0;
			// Y variable for the 2d array
			int maxY = 0;

			// Calculate max X and max Y for 2darray
			worldModel.steps.ForEach(step => {
				maxX = Math.Max(maxX, step.x);
				maxY = Math.Max(maxY, step.y);
			});

			// Creat world size 
			world.World2darray = new Step[maxY + 1, maxX + 1];

			// Use the models to create the usable septs.
			worldModel.steps.ForEach(step => {
				// Create the step.
				Step newStep = new Step() {
					Title = step.title,
					X = step.x,
					Y = step.y,
					Description = step.description,
					// Set the four possible directions.
					AccessFrom = new Dictionary<string, bool>() {
						{"top", step.accessfrom.top},
						{"right", step.accessfrom.right},
						{"bottom", step.accessfrom.bottom},
						{"left", step.accessfrom.left}
					},
					// Spacing 
					SpaceType = (step.spaceType == "ending" ? SpaceType.End :
						step.spaceType == "start" ? SpaceType.Start : SpaceType.Normaal),
					// Event
					ThisEvent = (step.stepEvent ? _eventBuilder.BuildRandomEvent() : null)
				};

				// Assign step to array position.
				world.World2darray[world.MapYtoArrayY(step.y), step.x] = newStep;

				// If step is the start, make it the (first) current step.
				if (newStep.SpaceType == SpaceType.Start) {
					world.CurrentStep = newStep;
				}
			});

			return world;
		}
	}
}