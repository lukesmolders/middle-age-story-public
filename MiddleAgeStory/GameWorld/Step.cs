﻿using System.Collections.Generic;
using middle_age_story.Characters;
using middle_age_story.Events;

namespace middle_age_story.GameWorld {
	/// <summary>
	/// Every Step is a part of the path of world.
	/// </summary>
	public class Step {
		// Coordinates
		public int X;
		public int Y;

		// Title
		public string Title;

		// ExtrInfo
		public string Description;

		// Accessfrom
		public Dictionary<string, bool> AccessFrom;

		// Event
		public Event ThisEvent;

		// Spacetype enum
		public SpaceType SpaceType;
	}
}

/// <summary>
/// Spacetype indicates which type of step this is
/// </summary>
public enum SpaceType {
	// Just a ordinary Step
	Normaal,

	// The start of the games
	Start,

	// Ending of the game
	End
}