﻿namespace middle_age_story.Weapons {
	/// <summary>
	/// Sword is one of the Weapons that can be used by a character
	/// </summary>
	public class Sword : Weapon {
		/// <summary>
		/// Initializes a new instance of the Sword class
		/// <param name="attack">Optional parameter for setting the attack strength</param>
		/// <param name="defense">Optional parameter for setting the defense strength</param>
		/// </summary>t
		public Sword(int attack = 8, int defense = 4) {
			Attack = attack;
			Defense = defense;
		}
	}
}