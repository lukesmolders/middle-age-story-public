﻿using middle_age_story.Weapons;

namespace middle_age_story.Weapons {
	/// <summary>
	/// Bow is one of the Weapons that can be used by a character
	/// </summary>
	public class Bow : Weapon {
		/// <summary>
		/// Initializes a new instance of the Bow class
		/// <param name="attack">Optional parameter for setting the attack strength</param>
		/// <param name="defense">Optional parameter for setting the defense strength</param>
		/// </summary>
		public Bow(int attack = 8, int defense = 4) {
			Attack = attack;
			Defense = defense;
		}
	}
}