﻿namespace middle_age_story.Weapons {
	/// <summary>
	/// Abstract class weapon will be used by all the weapons for the characters
	/// </summary>
	public abstract class Weapon {
		/// <summary>
		/// When a character opponent attacks, Defense will be used to check how much protection this weapon gives.
		/// </summary>
		public int Defense { get; set; }

		/// <summary>
		/// When a character attacks , Attack will be used to check how much damage the character can do with this weapon 
		/// </summary>
		public int Attack { get; set; }
	}
}