﻿namespace middle_age_story.Weapons {
	/// <summary>
	/// Staff is one of the Weapons that can be used by a character
	/// </summary>
	public class Staff : Weapon {
		/// <summary>
		/// Initializes a new instance of the Staff class
		/// <param name="attack">Optional parameter for setting the attack strength</param>
		/// <param name="defense">Optional parameter for setting the defense strength</param>
		/// </summary>
		public Staff(int attack = 5, int defense = 5) {
			Attack = attack;
			Defense = defense;
		}
	}
}