﻿using System;
using System.Collections.Generic;

namespace middle_age_story.Weapons {
	/// <summary>
	/// WeaponFactory creates weapons
	/// </summary>
	public class WeaponFactory {
		private static WeaponFactory _instance;

		// Delegate in order to dynamically create weapons
		public delegate Weapon CreateWeapon();

		private readonly Dictionary<string, CreateWeapon> _weapons;

		/// <summary>
		/// Initializes a new instance of the Weaponfactory
		/// </summary>
		private WeaponFactory() {
			// Initiate
			_weapons = new Dictionary<string, CreateWeapon>();
			// Register weapon
			RegisterWeapons();
		}

		/// <summary>
		/// Instance makes sure that there is always just one Instance of WeaponFactory
		/// If there is no WeaponFactory, instance will create one and return that
		/// else the current WeaponFactory instance will be returned.
		/// </summary>
		public static WeaponFactory Instance {
			get {
				if (_instance == null) {
					_instance = new WeaponFactory();
				}

				return _instance;
			}
		}
		
		/// <summary>
		/// RegisterWeapons will register all the predefined weapons that factory can make
		/// </summary>
		private void RegisterWeapons() {
			RegisterNewWeapon("staff", () => new Staff());
			RegisterNewWeapon("bow", () => new Bow());
			RegisterNewWeapon("sword", () => new Sword());
		}

		/// <summary>
		/// RegisterNewWeapon can be used to add a new weapon for the weaponFactory to make
		/// </summary>
		/// <param name="weaponFunc">Delegate pointer to function</param>
		/// <returns></returns>
		public bool RegisterNewWeapon(string weaponName, CreateWeapon weaponFunc) {
			// Check if there is input
			if (weaponName == null) {
				return false;
			}

			// Check if the weapon doesnt already exist
			if (_weapons.ContainsKey(weaponName)) {
				return false;
			}

			// All checks out, add weapon
			_weapons.Add(weaponName, weaponFunc);

			return true;
		}

		/// <summary>
		/// Look if the requested weapon is one the weaponfactory can build
		/// </summary>
		/// <param name="weaponName"></param>
		/// <returns>The name of the weapon</returns>
		public string SearchForKnownWeapons(string weaponName) {
			return _weapons.ContainsKey(weaponName) ? weaponName : null;
		}

		/// <summary>
		/// Create new Weapon instance
		/// </summary>
		/// <param name="weaponName">weaponName</param>
		/// <returns>The new Weapon instance</returns>
		public Weapon Create(string weaponName) {
			// Make weaponName lowercase
			string lowerCase = weaponName.ToLowerInvariant();

			// Check if weaponName is known
			if (_weapons.ContainsKey(lowerCase)) {
				// Return that Weapon
				return _weapons[lowerCase].Invoke();
			}

			return null;
		}
	}
}