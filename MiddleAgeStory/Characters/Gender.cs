namespace middle_age_story.Characters {
	/// <summary>
	/// Genders for Player
	/// </summary>
	public class Gender {
		public readonly string Type;
		public readonly string Title;

		/// <summary>
		/// Initializes a new instance of the Gender class
		/// </summary>
		/// <param name="type">The type of gender</param>
		/// <param name="title">The title this gender gets called by</param>
		public Gender(string type, string title) {
			this.Type = type;
			this.Title = title;
		}
	}
}