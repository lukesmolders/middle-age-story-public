﻿using middle_age_story.Characters.Attack;
using middle_age_story.Weapons;

namespace middle_age_story.Characters {
	/// <summary>
	/// A character that can be choosen by player
	/// </summary>
	public class Swordman : Character {
		/// <summary>
		/// Initializes a new instance of the swordman class
		/// </summary>
		/// <param name="weaponFactory">Weaponfactory to create weapon for character</param>
		public Swordman(WeaponFactory weaponFactory) : base(weaponFactory) {
			Health = 28;
			Weapon = weaponFactory.Create("sword");
			AttackYell = "Pak aan schavuit!, ... \n *Slaat met zwaard";
			AttackStrategy = new SwordAttack();
		}
	}
}