using System;
using System.Collections.Generic;
using System.Linq;

namespace middle_age_story.Characters {
	/// <summary>
	/// Genderpicker makes it possible to ask the player which gender he is and add new genders programmicly if necessary
	/// Default genders are: man, vrouw and onzijdig.
	/// </summary>
	public class GenderPicker {
		private readonly Dictionary<string, string> Genders;

		private static GenderPicker _instance;

		/// <summary>
		/// Initializes a new instance of the GenderPicker class
		/// </summary>
		private GenderPicker() {
			// Creates three default Genders
			Genders = new Dictionary<string, string> {{"man", "Sir"}, {"vrouw", "Madam"}, {"onzijdig", "Individu"}};
		}

		/// <summary>
		/// Instance makes sure that there is always just one Instance of GenderPicker
		/// If there is no GenderPicker, instance will create one and return that
		/// else the current GenderPicker instance will be returned.
		/// </summary>
		public static GenderPicker Instance {
			get {
				if (_instance == null) {
					_instance = new GenderPicker();
				}

				return _instance;
			}
		}

		/// <summary>
		/// Get gender from player input
		/// </summary>
		/// <param name="inputOutput">This is the console to output to the user</param>
		/// <returns></returns>
		// Get gender from player input
		public Gender GetGenderFromPlayerInput(IInputOutput inputOutput) {
			string playerGender = "";
			string playerTitle = "";
			string inputFromPlayer;
			string possibleGenders =
				Genders.Keys.Aggregate((current, possibleCharacters) => current + ", " + possibleCharacters);
			;


			do {
				// Ask player which gender he or she is.
				inputFromPlayer = inputOutput
					.GetInput(new string[] {$"Welk geslacht bent u? ({possibleGenders})"}, false).ToLower();

				// Check if input matches 
				if (Genders.ContainsKey(inputFromPlayer)) {
					playerGender = inputFromPlayer;
					playerTitle = Genders[inputFromPlayer];
				}

				// If the input does not match, tell the player.
				else {
					inputOutput.Print(new string[] {"Sorry dat is geen geslacht."}, false);
				}

				// Keep on going with the loop while the player input does not match any of genders a player can have.
			} while (!Genders.Any(
				g => g.Key.Equals(inputFromPlayer, StringComparison.InvariantCultureIgnoreCase)));

			return new Gender(playerGender, playerTitle);
		}

		/// <summary>
		/// Add new gender to genders
		/// </summary>
		/// <param name="gender">name of the gender</param>
		/// <param name="title">Title to use when talking to the gender.</param>
		public void AddGender(string gender, string title) {
			Genders.Add(gender, title);
		}
	}
}