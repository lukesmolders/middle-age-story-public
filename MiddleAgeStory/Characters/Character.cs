﻿using middle_age_story.Characters.Attack;
using middle_age_story.Enemies;
using middle_age_story.Weapons;

namespace middle_age_story.Characters {
	/// <summary>
	///  The abstract class character which specific characters can use to inherit from
	/// </summary>
	public abstract class Character {
		/// <summary>
		/// Every Character needs a weapon
		/// </summary>
		public Weapon Weapon { get; protected set; }

		/// <summary>
		/// Every Character has Health
		/// </summary>
		public int Health { get; protected set; }

		/// <summary>
		/// AttackYell is the string that gets returned to console when attack is being used.
		/// </summary>
		public string AttackYell { get; protected set; }

		/// <summary>
		/// AttackStrategy is used for strategy pattern and is the way the character is attacking
		/// </summary>
		protected IAttack AttackStrategy { get; set; }

		/// <summary>
		/// Initializes a new instance of the Character class
		/// </summary>
		/// <param name="weaponFactory">weaponFactory to let the character get its weapon</param>
		public Character(WeaponFactory weaponFactory) {
		}

		/// <summary>
		/// AttackMOve is the character performing a attack on the enemy.
		/// </summary>
		/// <param name="iinputOutput">This is the console to output to the user</param>
		/// <param name="enemy">The enemy that gets attacked by player</param>
		public void AttackMove(IInputOutput iinputOutput, Enemy enemy) {
			AttackStrategy.Attack(iinputOutput, enemy, this);
		}

		/// <summary>
		/// When the character gets attacked, DamageTaken gets called to adjust the character its health.
		/// </summary>
		/// <param name="damage">The amount of damage the character has taken</param>
		public void DamageTaken(int damage) {
			Health -= damage;
		}

		/// <summary>
		/// Feedback to player with all stats
		/// </summary>
		/// <param name="inputOutputManager">This is the console to output to the user</param>
		public void CurrentStatus(IInputOutput inputOutputManager) {
			inputOutputManager.PrintDescription(new string[] {
					"Overzicht:",
					$"Character: {GetType().Name}",
					$"Gezondheid: {Health.ToString()} hartjes",
					$"Wapen: {Weapon.GetType().Name}"
				},
				false);
		}

		/// <summary>
		/// AddHealth to character
		/// Current health + health
		/// </summary>
		/// <param name="health">Health to add</param>
		public void AddHealth(int health) {
			if (health > 0) {
				Health += health;
			}
		}
	}
}