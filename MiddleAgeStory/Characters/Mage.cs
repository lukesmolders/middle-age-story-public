﻿using middle_age_story.Characters.Attack;
using middle_age_story.Weapons;

namespace middle_age_story.Characters {
	/// <summary>
	///  A character that can be choosen by player
	/// </summary>
	public class Mage : Character {
		/// <summary>
		/// Initializes a new instance of the Mage class
		/// </summary>
		/// <param name="weaponFactory">Weaponfactory to create weapon for character</param>
		public Mage(WeaponFactory weaponFactory) : base(weaponFactory) {
			Health = 30;
			Weapon = weaponFactory.Create("staff");
			AttackYell = "simsalabim in je hol vlegel!,.... \n *Vuurt vuurbal af met staf";
			AttackStrategy = new MageAttack();
		}
	}
}