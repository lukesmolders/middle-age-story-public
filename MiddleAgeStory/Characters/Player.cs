﻿using System.Collections.Generic;
using middle_age_story.Items;

namespace middle_age_story.Characters {
	/// <summary>
	/// Player class this class is responsible for everything the player has
	/// </summary>
	public class Player {
		private readonly string _name;
		private readonly Character _character;

		private readonly Gender _gender;

		public List<Item> Items { get; private set; }

		/// <summary>
		/// Initializes a new instance of the Player
		/// </summary>
		/// <param name="name">Name of the player</param>
		/// <param name="gender">The gender of the player</param>
		/// <param name="character">The character of the player</param>
		public Player(string name, Gender gender, Character character) {
			_name = name;
			_gender = gender;
			_character = character;
			Items = new List<Item>();
		}

		/// <summary>
		/// Return the name of the player
		/// </summary>
		/// <returns>Player name</returns>
		public string GetName() {
			return $"{_name} {_gender.Title}";
		}

		/// <summary>
		/// Return the character of the player.
		/// </summary>
		/// <returns>Player Character</returns>
		public Character GetCharacter() {
			return _character;
		}

		/// <summary>
		/// Add item to players items
		/// </summary>
		/// <param name="item">Item to add.</param>
		public void AddItem(Item item) {
			Items.Add(item);
		}

		/// <summary>
		/// Player uses the item
		/// </summary>
		/// <param name="item">Item to be used</param>
		public void UseItem(Item item) {
			// Add health to character
			_character.AddHealth(item.ReturningHealth);
			// Remove item from inventory
			Items.Remove(item);
		}
	}
}