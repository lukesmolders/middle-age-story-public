﻿using middle_age_story.Characters.Attack;
using middle_age_story.Weapons;

namespace middle_age_story.Characters {
	/// <summary>
	/// A character that can be choosen by player
	/// </summary>
	public class Bowman : Character {
		/// <summary>
		/// Initializes a new instance of the Bowman class
		/// </summary>
		/// <param name="weaponFactory">Weaponfactory to create weapon for character</param>
		public Bowman(WeaponFactory weaponFactory) : base(weaponFactory) {
			Health = 25;
			Weapon = weaponFactory.Create("bow");
			AttackYell = "Eet dit doerak!!,.... \n*Schiet pijl af*";
			AttackStrategy = new RangeAttack();
		}
	}
}