﻿using System.Collections.Generic;
using middle_age_story.Weapons;

namespace middle_age_story.Characters {
	/// <summary>
	/// Factory to create new characters
	/// </summary>
	public class CharacterFactory {
		// Instance for singleton
		private static CharacterFactory _instance;

		// List with all available characters
		public Dictionary<string, CreateCharacter> Characters { get; private set; }

		private readonly WeaponFactory _weaponFactory;

		public delegate Character CreateCharacter();

		/// <summary>
		/// Initializes a new instance of the CharacterFactory class
		/// </summary>
		private CharacterFactory() {
			_weaponFactory = WeaponFactory.Instance;
			// Initiate
			Characters = new Dictionary<string, CreateCharacter>();
			// Register characters
			RegisterCharacters();
		}

		/// <summary>
		/// Instance makes sure that there is always just one Instance of CharacterFactory
		/// If there is no CharacterFactory, instance will create one and return that
		/// else the current CharacterFactory instance will be returned.
		/// </summary>
		public static CharacterFactory Instance {
			get {
				if (_instance == null) {
					_instance = new CharacterFactory();
				}

				return _instance;
			}
		}

		/// <summary>
		/// Adds default character options
		/// </summary>
		private void RegisterCharacters() {
			RegisterNewCharacter("bowman", () => new Bowman(_weaponFactory));
			RegisterNewCharacter("swordman", () => new Swordman(_weaponFactory));
			RegisterNewCharacter("mage", () => new Mage(_weaponFactory));
		}

		/// <summary>
		/// Add a new character to the factory
		/// </summary>
		/// <param name="characterName">Name of the character</param>
		/// <typeparam name="T">Class of character</typeparam>
		/// <returns></returns>
		public bool RegisterNewCharacter(string characterName, CreateCharacter characterFunc) {
			// Check if there is input
			if (characterName == null) {
				return false;
			}

			// Check if the character doesnt already exist
			if (Characters.ContainsKey(characterName)) {
				return false;
			}

			// All checks out, add character
			Characters.Add(characterName, characterFunc);

			return true;
		}

		/// <summary>
		/// Check if known in Dictonary
		/// </summary>
		/// <param name="characterName">Character name to be searched.</param>
		/// <returns>String with corrisponding charactername</returns>
		public string SearchForKnownCharacters(string characterName) {
			// Check if characterName is found in dictonary, if so return that characterName els, return null
			return Characters.ContainsKey(characterName) ? characterName : null;
		}

		/// <summary>
		///     Create a new character instance.
		/// </summary>
		/// <param name="characterName">Name of the character</param>
		/// <returns>ICharacter</returns>
		public Character Create(string characterName) {
			// Make characterName lowercase
			string lowerCase = characterName.ToLowerInvariant();

			// Check if characterName is known
			if (Characters.ContainsKey(lowerCase)) {
				// Return that character
				return Characters[lowerCase].Invoke();
			}

			return null;
		}
	}
}