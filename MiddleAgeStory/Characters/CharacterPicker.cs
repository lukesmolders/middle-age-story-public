using System;
using System.Linq;

namespace middle_age_story.Characters {
	/// <summary>
	/// CharacterPicker makes it possible to ask the player which character he is
	/// </summary>
	public static class CharacterPicker {
		/// <summary>
		/// This function creates the character that the player wants
		/// </summary>
		/// <param name="inputOutput">This is the console to output to the user</param>
		/// <param name="characterFactory">This is the CharacterFactory to create a new character</param>
		/// <returns></returns>
		public static Character CreaterCharacterFromPlayerInput(IInputOutput inputOutput,
			CharacterFactory characterFactory) {
			Character character = null;
			string allPosibleCharacters =
				// Aggregate loops thru all values but remembers what has already been added together.
				characterFactory.Characters.Keys.Aggregate((current, possibleCharacters) =>
					current + ", " + possibleCharacters);

			do {
				// Ask player which gender he or she is.
				string inputFromPlayer =
					inputOutput.GetInput(new string[] {$"Welke personage wil je zijn?: {allPosibleCharacters} "}, false)
						.ToLower();

				// Check if input matches 
				if (inputFromPlayer.Equals(characterFactory.SearchForKnownCharacters(inputFromPlayer),
					StringComparison.InvariantCultureIgnoreCase)) {
					// Create character using characterFactory
					character = characterFactory.Create(inputFromPlayer);
				}
				else {
					// If input is not a valid character, ask again
					inputOutput.Print(new string[] {"Dit karakter ken ik niet, welke bedoelde je?"}, false);
				}

				// Keep on going with the loop while the player input does not match any of genders a player can have.
			} while (character == null);

			// Return the chosen character
			return character;
		}
	}
}