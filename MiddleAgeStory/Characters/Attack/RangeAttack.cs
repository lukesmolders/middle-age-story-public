﻿using System;
using System.Timers;
using middle_age_story.Enemies;
using middle_age_story.GameWorld;

namespace middle_age_story.Characters.Attack {
	/// <summary>
	/// IAttack implementation for Bowman
	/// </summary>
	public class RangeAttack : IAttack {
		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="character">Character that player picked</param>
		/// <param name="inputOutput">iinputOuput for letting player know whats up.</param>
		/// <param name="enemy">The enemy to be attacked</param>
		public void Attack(IInputOutput inputOutput, Enemy enemy, Character character) {
			inputOutput.Print(new string[] {character.AttackYell}, false);

			character.DamageTaken(enemy.AttackDamage);

			Random random = new Random();
			int hitArrows = random.Next(1, 3);

			for (int i = 0; i < hitArrows; i++) {
				inputOutput.Print(new string[] {"*Schiet nog een pijl raak*"}, false);
				character.DamageTaken(enemy.AttackDamage);
				enemy.TakeDamage(character.Weapon.Attack);

				GameMomentum.Delay(800);
			}
		}
	}
}