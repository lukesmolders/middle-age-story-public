﻿using middle_age_story.Enemies;

namespace middle_age_story.Characters.Attack {
	/// <summary>
	/// IAttack interface for Characters
	/// </summary>
	public interface IAttack {
		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="character">Character that player picked</param>
		/// <param name="enemy">Enemy to attack</param>
		/// <param name="inputOutput">iinputOuput for letting player know whats up.</param>
		public void Attack(IInputOutput inputOutput, Enemy enemy, Character character);
	}
}