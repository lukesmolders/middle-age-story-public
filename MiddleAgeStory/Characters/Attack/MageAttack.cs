﻿using middle_age_story.Enemies;

namespace middle_age_story.Characters.Attack {
	/// <summary>
	/// IAttack implementation for Mage
	/// </summary>
	public class MageAttack : IAttack {
		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="character">Character that player picked</param>
		/// <param name="enemy">The enemy to be attacked</param>
		/// <param name="inputOutput">iinputOuput for letting player know whats up.</param>
		public void Attack(IInputOutput inputOutput, Enemy enemy, Character character) {
			inputOutput.Print(new string[] {character.AttackYell}, false);

			character.DamageTaken(enemy.AttackDamage);

			enemy.SetHealth(enemy.Health - character.Weapon.Attack);
		}
	}
}