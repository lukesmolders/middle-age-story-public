﻿using System;
using System.IO;
using middle_age_story.Properties;

namespace middle_age_story {
	/// <summary>
	/// Implementation of the inputOutput.
	/// </summary>
	public class InputOutputManager : IInputOutput {
		/// <summary>
		/// PrintToScreen output to the screen.
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		public void Print(string[] lines, bool clear) {
			// Empty the console
			if (clear) {
				ClearConsole();
			}

			// For each line in lines, print that line
			foreach (string line in lines) {
				Console.WriteLine($"{line}");
			}
		}

		/// <summary>
		/// Outputs to screen in a more fancy way 
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		public void PrintDescription(string[] lines, bool clear) {
			// Empty the console
			if (clear) {
				ClearConsole();
			}

			// Check which line is the longest
			int longestLine = FindLongestLine(lines);

			// Create header string
			string borderLine = new String('-', longestLine + 2);

			// write this block.
			Console.WriteLine($"/{borderLine}\\");

			// print each line
			// padright returns a new string of specified length
			// longestLine + space + '|'
			foreach (string line in lines) {
				Console.WriteLine($"| {line.PadRight(longestLine, ' ')} |");
			}

			Console.WriteLine($"\\{borderLine}/");
		}

		/// <summary>
		/// PrintToScreen to screen but also return the input from user.
		/// </summary>
		/// <param name="lines">Text to print, each array item is a new line</param>
		/// <param name="clear">bool to clear or not clear the console</param>
		/// <returns>Input from playuer</returns>
		public string GetInput(string[] lines, bool clear) {
			foreach (string line in lines) {
				Console.WriteLine($"{line}");
			}

			// Return input from user.
			return Console.ReadLine();
		}

		/// <summary>
		/// Print the welcome logo with optional message
		/// </summary>
		/// <param name="message">The message to display with the logo</param>
		public void PrintWelcome(string message) {
			Console.WriteLine(Resources.Logo);
			Console.WriteLine(message);
		}

		/// <summary>
		/// Empty the console
		/// </summary>
		public void ClearConsole() {
			Console.Clear();
		}

		/// <summary>
		/// This calculates the size of the output , so fancy way of presentation is possible
		/// </summary>
		/// <param name="lines">Lines to check</param>
		/// <returns></returns>
		private int FindLongestLine(string[] lines) {
			int longest = 0;
			foreach (string line in lines) {
				longest = Math.Max(longest, line.Length);
			}

			return longest;
		}
	}
}