using middle_age_story.Events;

namespace middle_age_story.Models {
	/// <summary>
	/// StepModel in order go get Step from JSON file
	/// </summary>
	public class StepModel {
		public int x;
		public int y;
		public string title;
		public string description;
		public string spaceType;
		public AllowedDirectionModel accessfrom;
		public bool stepEvent;
	}
}