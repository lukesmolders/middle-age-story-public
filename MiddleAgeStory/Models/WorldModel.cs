using System.Collections.Generic;

namespace middle_age_story.Models {
	/// <summary>
	/// List of all the steps in order go get them from JSON file
	/// </summary>
	public class WorldModel {
		public List<StepModel> steps;
	}
}