namespace middle_age_story.Models {
	/// <summary>
	/// Directions for the Steps from JSON file
	/// </summary>
	public class AllowedDirectionModel {
		public bool top;
		public bool bottom;
		public bool left;
		public bool right;
	}
}