﻿using middle_age_story.Characters;

namespace middle_age_story.Enemies {
	/// <summary>
	/// StateMachine has control of the states 
	/// </summary>
	public class StateMachine {
		// Current Enemy state
		private EnemyState _enemyState;

		/// <summary>
		/// Set the first enemyState
		/// </summary>
		/// <param name="enemyState">The new EnemyState</param>
		public StateMachine(EnemyState enemyState) {
			TransitionTo(enemyState);
		}

		/// <summary>
		/// transition to another state
		/// </summary>
		/// <param name="newState">New State</param>
		public void TransitionTo(EnemyState newState) {
			// Set the state
			_enemyState = newState;
			// Set the statemachine in enemystate
			newState.SetStateMachineBase(this);
			newState.OnStateEnter();
		}

		/// <summary>
		/// Attack the character!
		/// </summary>
		/// <param name="enemy">The Enemy that is going to attack</param>
		/// <param name="character">The character that gets attacked</param>
		/// <param name="iinputOutput">This is the console to output to the user</param>
		public void Attack(Enemy enemy, Character character, IInputOutput iinputOutput) {
			_enemyState.Attack(enemy, character, iinputOutput);
		}
	}
}