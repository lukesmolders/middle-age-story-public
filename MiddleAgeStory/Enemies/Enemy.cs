﻿using middle_age_story.Characters;
using middle_age_story.Enemies.States;

namespace middle_age_story.Enemies {
	/// <summary>
	/// Enemy is a oponent to the character. 
	/// </summary>
	public class Enemy {
		private readonly StateMachine _stateMachine;
		public int AttackDamage { get; private set; }
		public int Health { get; private set; }
		private readonly string _name;

		/// <summary>
		/// Initializes a new instance of the Enemy class
		/// </summary>
		/// <param name="name">The name of the Enemy instance</param>
		public Enemy(string name) {
			_name = name;
			// When Enemy is created he will be unaware of hostillities surrounding him.
			_stateMachine = new StateMachine(new UnawareState());
		}

		/// <summary>
		/// Set the current amount of health for the Enemy.
		/// </summary>
		/// <param name="health">integer, amount of health</param>
		public void SetHealth(int health) {
			Health = health;
		}

		/// <summary>
		/// Set the current amount of attackdamage of Enemy.
		/// </summary>
		/// <param name="damage">integer, amount of attackdamage</param>
		public void SetAttackDamage(int damage) {
			AttackDamage = damage;
		}

		/// <summary>
		/// Return integer with current attackdamage of Enemy.
		/// </summary>
		/// <returns>integer, amount of attackdamage</returns>
		public string GetName() {
			return _name;
		}

		/// <summary>
		/// AttackAware makes the Enemy transitionTo attack state.
		/// </summary>
		public void AttackAware() {
			_stateMachine.TransitionTo(new AttackState());
		}

		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="character">Character that player picked</param>
		/// <param name="iinputOutput">iinputOuput for letting player know whats up.</param>
		public void Attack(Character character, IInputOutput iinputOutput) {
			_stateMachine.Attack(this, character, iinputOutput);
		}

		/// <summary>
		/// When the Enemy gets attacked, his health will decrease with the amount that the oponent his weapon deals damage
		/// </summary>
		/// <param name="weaponAttack">Oponent weapon damage</param>
		public void TakeDamage(int weaponAttack) {
			SetHealth(Health - weaponAttack);
		}
	}
}