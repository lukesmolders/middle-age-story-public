﻿using middle_age_story.Characters;

namespace middle_age_story.Enemies {
	/// <summary>
	///     Abstract class for statemachine in Enemy
	/// </summary>
	public abstract class EnemyState {
		// Hold the statemachine
		protected StateMachine StateMachine;

		// Set the statemachine.
		/// <summary>
		/// Set the StateMachine
		/// </summary>
		/// <param name="stateMachine">StateMachine</param>
		public void SetStateMachineBase(StateMachine stateMachine) {
			StateMachine = stateMachine;
		}

		/// <summary>
		/// Attack to be called by statemachine
		/// </summary>
		/// <param name="enemy">The Enemy that is going to attack</param>
		/// <param name="character">The character that gets attacked</param>
		/// <param name="inputOutput">This is the console to output to the user</param>
		public abstract void Attack(Enemy enemy, Character character, IInputOutput inputOutput);

		/// <summary>
		/// Everythime the state gets set, OnStateEnter will be called.
		/// </summary>
		public abstract void OnStateEnter();
	}
}