﻿using middle_age_story.Characters;

namespace middle_age_story.Enemies.States {
	/// <summary>
	/// The UnawareState is a Enemystate
	/// UnawareState has nothing to do, so will just return.
	/// </summary>
	public class UnawareState : EnemyState {
		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="enemy">The enemy that is going to attack</param>
		/// <param name="character">The opponent for the Enemy</param>
		/// <param name="inputOutput">This is the console to output to the user</param>
		public override void Attack(Enemy enemy, Character character, IInputOutput inputOutput) {
			// Return because the Enemy is still unaware
			return;
		}

		/// <summary>
		///  OnStateEnter gets called when the state is being transitioned.
		/// </summary>
		public override void OnStateEnter() {
		}
	}
}