﻿using middle_age_story.Characters;

namespace middle_age_story.Enemies.States {
	/// <summary>
	/// When an enemy uses his attack
	/// </summary>
	public class AttackState : EnemyState {
		private const int SWITCHAFTER = 3;
		private const int MULTIPLE = 3;
		private int _attackCounter;

		/// <summary>
		/// Enemy attacks the character
		/// </summary>
		/// <param name="enemy">The enemy that is going to attack</param>
		/// <param name="character">The opponent for the Enemy</param>
		/// <param name="inputOutput">This is the console to output to the user</param>
		public override void Attack(Enemy enemy, Character character, IInputOutput inputOutput) {
			_attackCounter++;
			// Set enemy attack damage to full attackmode
			enemy.SetAttackDamage(enemy.AttackDamage * MULTIPLE);

			// Check amount of attacks, if its equal to SwitchAfter, than engage exhausted
			if (_attackCounter == SWITCHAFTER) {
				// Transition statemachine to exhausted state
				StateMachine.TransitionTo(new ExhaustedState());
			}

			enemy.SetAttackDamage(enemy.AttackDamage / MULTIPLE);
		}

		/// <summary>
		/// OnStateEnter gets called when the state is being transitioned.
		/// </summary>
		public override void OnStateEnter() {
			_attackCounter = 0;
		}
	}
}