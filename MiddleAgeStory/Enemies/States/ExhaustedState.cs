﻿using middle_age_story.Characters;

namespace middle_age_story.Enemies.States {
	/// <summary>
	/// When an enemy uses his attack to much he will get exhausted and his attack power decreases.
	/// </summary>
	public class ExhaustedState : EnemyState {
		private const int SWITCHAFTER = 2;
		private int _attackCounter;
		private readonly int _attackPowerDecrease = 3;

		/// <summary>
		/// Attack makes the enemy attack the character.
		/// </summary>
		/// <param name="enemy">The enemy that is going to attack</param>
		/// <param name="character">The opponent for the Enemy</param>
		/// <param name="inputOutput">This is the console to output to the user</param>
		public override void Attack(Enemy enemy, Character character, IInputOutput inputOutput) {
			_attackCounter++;
			// Set enemy attack damage / 3
			enemy.SetAttackDamage(enemy.AttackDamage / _attackPowerDecrease);

			// Check amount of attacks, if its equal to SwitchAfter, than engage exhausted
			if (_attackCounter == SWITCHAFTER) {
				// Transition statemachine to attack state
				StateMachine.TransitionTo(new AttackState());
			}
		}

		/// <summary>
		/// OnStateEnter gets called when the state is being transitioned.
		/// </summary>
		public override void OnStateEnter() {
			_attackCounter = 0;
		}
	}
}