using middle_age_story;
using middle_age_story.Characters;
using middle_age_story.Items;
using middle_age_story.Weapons;
using Moq;
using Xunit;

namespace MiddleAgeStory.Test {
	public class PlayerTests {
		[Fact]
		public void PlayerTests_Constructor() {
			// Arrange
			string name = "Dodo";
			Gender man = new Gender("man", "sire");
			Bowman character = new Bowman(WeaponFactory.Instance);

			// Act
			Player player = new Player(name, man, character);

			// Assert
			Assert.IsType<Player>(player);
		}

		[Fact]
		public void PlayerTests_GetName() {
			// Arrange
			string name = "Dodo";
			string totalname = "Dodo Dodo";
			Gender gender = new Gender("man", "Dodo");
			Bowman character = new Bowman(WeaponFactory.Instance);

			// Act
			Player player = new Player(name, gender, character);

			// Assert
			Assert.Equal(totalname, player.GetName());
		}

		[Fact]
		public void PlayerTests_GetCharacter() {
			// Arrange
			string name = "Dodo";
			string totalname = "Dodo Dodo";
			Gender gender = new Gender("man", "Dodo");
			Bowman character = new Bowman(WeaponFactory.Instance);

			// Act
			Player player = new Player(name, gender, character);

			// Assert
			Assert.Equal(character, player.GetCharacter());
		}

		[Fact]
		public void PlayerTests_Add_Item() {
			// Arrange
			string name = "Dodo";
			Gender gender = new Gender("man", "Dodo");
			Bowman character = new Bowman(WeaponFactory.Instance);
			Item item = new Item(ItemType.apple, 50);

			// Act
			Player player = new Player(name, gender, character);
			player.AddItem(item);

			// Assert
			Assert.Contains(item, player.Items);
		}


		[Fact]
		public void PlayerTests_Use_Item() {
			// Arrange
			string name = "Dodo";
			Gender gender = new Gender("man", "Dodo");
			Bowman character = new Bowman(WeaponFactory.Instance);
			int previousHealth = character.Health;
			Item item = new Item(ItemType.apple, 50);

			// Act
			Player player = new Player(name, gender, character);
			player.AddItem(item);
			player.UseItem(item);

			// Assert
			Assert.True(previousHealth < player.GetCharacter().Health);
		}
	}
}