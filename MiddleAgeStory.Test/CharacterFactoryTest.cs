﻿using middle_age_story.Characters;
using middle_age_story.Weapons;
using MiddleAgeStory.Test.MockImplementations;
using Xunit;

namespace MiddleAgeStory.Test {
	public class CharacterFactoryTest {
		/// <summary>
		/// function to use as delegate
		/// </summary>
		/// <returns></returns>
		public Character MakeFake() {
			WeaponFactory weaponFactory = WeaponFactory.Instance;

			return new MockCharacter(weaponFactory);
		}

		[Fact]
		public void CharacterFactory_RegisterNewCharacter() {
			// Arrange
			CharacterFactory characterFactory = CharacterFactory.Instance;
			string characterName = "James Bond";

			// Act
			characterFactory.RegisterNewCharacter(characterName, MakeFake);

			// Assert
			Assert.IsType<MockCharacter>(MakeFake());
			Assert.Equal(characterName, characterFactory.SearchForKnownCharacters(characterName));
		}

		[Fact]
		public void CharacterFactory_RegisterNewCharacter_Allready_Created() {
			// Arrange
			CharacterFactory characterFactory = CharacterFactory.Instance;
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string characterName = "James Bond";
			bool expected = false;

			// Act
			characterFactory.RegisterNewCharacter(characterName, MakeFake);

			// Assert
			Assert.Equal(expected, characterFactory.RegisterNewCharacter(characterName, MakeFake));
		}

		[Fact]
		public void CharacterFactory_RegisterNewCharacter_Empty_CharacterName() {
			// Arrange
			CharacterFactory characterFactory = CharacterFactory.Instance;
			bool expected = false;

			// Act

			// Assert
			Assert.Equal(expected, characterFactory.RegisterNewCharacter(null, MakeFake));
		}

		[Fact]
		public void CharacterFactory_Create() {
			// Arrange
			CharacterFactory characterFactory = CharacterFactory.Instance;
			string characterName = "swordman";
			Character character = null;

			// Act
			character = characterFactory.Create(characterName);

			// Assert
			Assert.Equal(typeof(Swordman), character.GetType());
		}

		[Fact]
		public void CharacterFactory_Create_Unknown_Character() {
			// Arrange
			CharacterFactory characterFactory = CharacterFactory.Instance;
			string characterName = "James Bond";
			Character character = null;

			// Act
			character = characterFactory.Create(characterName);

			// Assert
			Assert.Null(character);
		}
	}
}