using middle_age_story;
using middle_age_story.Characters;
using MiddleAgeStory.Test.MockImplementations;
using Moq;
using Xunit;

namespace MiddleAgeStory.Test {
	public class GenderTests {
		[Theory]
		[InlineData("man")]
		[InlineData("vrouw")]
		[InlineData("onzijdig")]
		public void GenderTests_GetGender(string input) {
			// Arrange
			GenderPicker genderPicker = GenderPicker.Instance;
			MockIOM mockIom = new MockIOM();

			// Act
			mockIom.InputReturns.Enqueue(input);
			Gender genderFromPlayerInput = genderPicker.GetGenderFromPlayerInput(mockIom);

			// Assert
			Assert.Equal(input, genderFromPlayerInput.Type);
		}

		[Fact]
		public void GenderTests_GetGender_FalseInput() {
			// Arrange
			GenderPicker genderPicker = GenderPicker.Instance;
			MockIOM mockIom = new MockIOM();

			// Act
			mockIom.InputReturns.Enqueue("fake");
			mockIom.InputReturns.Enqueue("man");
			Gender genderFromPlayerInput = genderPicker.GetGenderFromPlayerInput(mockIom);

			// Assert
			Assert.Equal("man", genderFromPlayerInput.Type);
		}

		[Fact]
		public void GenderTests_AddGender() {
			// Arrange
			string newGender = "panda";
			string title = "lief beertje";
			GenderPicker genderPicker = GenderPicker.Instance;
			MockIOM mockIom = new MockIOM();

			// Act
			mockIom.InputReturns.Enqueue(newGender);
			genderPicker.AddGender(newGender, title);
			Gender genderFromPlayerInput = genderPicker.GetGenderFromPlayerInput(mockIom);

			// Assert
			Assert.Equal(newGender, genderFromPlayerInput.Type);
		}
	}
}