using System.Diagnostics.Tracing;
using middle_age_story.Events;
using middle_age_story.GameWorld;
using middle_age_story.Items;
using Xunit;

namespace MiddleAgeStory.Test {
	public class EventTests {
		[Fact]
		public void Event_BuildEvent() {
			// Arrange
			Event events;
			EventBuilder eventBuilder = new EventBuilder();
			int health = 55;
			int enemyDamage = 10;
			int itemHealth = 5;
			ItemType itemType = ItemType.apple;

			// Act 
			events = eventBuilder.BuildEvent(health, enemyDamage, itemHealth, itemType);

			// Assert
			Assert.Equal(health, events.Enemy.Health);
			Assert.Equal(itemHealth, events.Item.ReturningHealth);
		}
	}
}