using System.Collections.Generic;
using System.Linq;
using middle_age_story;

namespace MiddleAgeStory.Test.MockImplementations {
	/// <summary>
	/// MockIOM makes it possible to test functions that implement the IInputOutput manager.
	/// </summary>
	public class MockIOM : IInputOutput {
		// Print function
		public List<string> Print_Calls = new List<string>();

		// GetInput
		public Queue<string> InputReturns = new Queue<string>();

		public List<string> InputReturns_Calls = new List<string>();

		// PrintWelcome
		public List<string> PrintWelcome_Calls = new List<string>();

		// ClearConsole
		public int ClearConsole_Counter = 0;

		// PrintDescription
		public List<string> PrintDescription_Calls = new List<string>();

		public void Print(string[] lines, bool clear) {
			Print_Calls.Add(StringArrayCombine(lines));
		}

		public string GetInput(string[] lines, bool clear) {
			foreach (string line in lines) {
				InputReturns_Calls.Add(line);
			}

			return InputReturns.Dequeue();
		}

		public void PrintWelcome(string message) {
			PrintWelcome_Calls.Add(message);
		}

		public void ClearConsole() {
			ClearConsole_Counter++;
		}

		public void PrintDescription(string[] lines, bool clear) {
			PrintDescription_Calls.Add(StringArrayCombine(lines));
		}

		private string StringArrayCombine(string[] lines) {
			return lines.Aggregate((prev, next) => prev + next);
		}
	}
}