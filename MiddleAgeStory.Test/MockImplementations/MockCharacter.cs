﻿using middle_age_story.Characters;
using middle_age_story.Weapons;

namespace MiddleAgeStory.Test.MockImplementations {
	/// <summary>
	/// MockCharacter for testing purpose
	/// </summary>
	public class MockCharacter : Character {
		public MockCharacter(WeaponFactory weaponFactory) : base(weaponFactory) {
			Weapon = weaponFactory.Create("sword");
		}
	}
}