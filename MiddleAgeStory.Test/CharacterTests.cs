﻿using System.Threading;
using middle_age_story;
using middle_age_story.Characters;
using middle_age_story.Enemies;
using middle_age_story.Weapons;
using MiddleAgeStory.Test.MockImplementations;
using Moq;
using Xunit;

namespace MiddleAgeStory.Test {
	public class CharacterTests {
		[Theory]
		[InlineData("bowman", typeof(Bowman))]
		[InlineData("mage", typeof(Mage))]
		[InlineData("swordman", typeof(Swordman))]
		public void CharacterTests_Create_Character(string input, object characterType) {
			// Arrange
			CharacterFactory characterFactory;
			MockIOM mockIom = new MockIOM();
			mockIom.InputReturns.Enqueue(input);

			// Act
			characterFactory = CharacterFactory.Instance;
			Character actual = CharacterPicker.CreaterCharacterFromPlayerInput(mockIom, characterFactory);

			int fd = actual.Health;
			// Assert
			Assert.Equal(characterType, actual.GetType());
		}

		[Fact]
		public void CharacterTests_Create_Character_FalseInput() {
			// Arrange
			CharacterFactory characterFactory;
			MockIOM mockIom = new MockIOM();
			
			// Act
			mockIom.InputReturns.Enqueue("fake");
			mockIom.InputReturns.Enqueue("bowman");
			characterFactory = CharacterFactory.Instance;
			Character actual = CharacterPicker.CreaterCharacterFromPlayerInput(mockIom, characterFactory);

			// Assert
			Assert.Equal(typeof(Bowman), actual.GetType());
		}

		[Fact]
		public void CharacterTests_Attack() {
			// Arrange
			MockIOM mockIom = new MockIOM();
			mockIom.InputReturns.Enqueue(" ");
			
			Character character = CharacterFactory.Instance.Create("bowman");
			Enemy enemy = new Enemy("Ork");

			// Act
			enemy.SetHealth(50);
			enemy.SetAttackDamage(20);

			character.AttackMove(mockIom, enemy);

			// Assert
			Assert.InRange(enemy.Health, -100, 49);
		}

		[Fact]
		public void CharacterTests_EnemyAttack() {
			// Arrange
			MockIOM mockIom = new MockIOM();
			mockIom.InputReturns.Enqueue(" ");
			
			Enemy enemy = new Enemy("ork");
			Character character = CharacterFactory.Instance.Create("mage");

			// Act
			enemy.SetHealth(10);
			enemy.SetAttackDamage(1);
			enemy.AttackAware();

			while (enemy.Health > 0) {
				character.AttackMove(mockIom, enemy);
			}

			// Assert
			Assert.InRange(enemy.Health, -100, 0);
		}
	}
}