﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using middle_age_story;
using middle_age_story.Characters;
using middle_age_story.GameWorld;
using middle_age_story.Weapons;
using MiddleAgeStory.Test.MockImplementations;
using Moq;
using Xunit;

namespace MiddleAgeStory.Test {
	public class WorldTests {
		private const string JSON = "testworld.json";

		public World CreateWorld() {
			WorldBuilder worldBuilder = new WorldBuilder();
			World _world = worldBuilder.Createworld(JSON);

			return _world;
		}

		[Fact]
		public void World_Gets_Created() {
			// Arrange
			World world;

			// Act
			world = CreateWorld();

			// Assert
			Assert.True(world != null);
		}

		[Fact]
		public void World_Has_Map() {
			// Arrange
			World world;

			// Act
			world = CreateWorld();

			// Assert
			Assert.True(world.World2darray.Length > 0);
		}

		[Fact]
		public void World_Get_Position() {
			// Arrange
			World world;
			int x = 1;
			int y = 1;
			string stepTitle = "begin";

			// Act
			world = CreateWorld();
			Step actualStep = world.GetStepByCords(x, y);

			// Assert
			Assert.Equal(stepTitle, actualStep.Title);
		}

		[Fact]
		public void World_Get_Position_Not_Found() {
			// Arrange
			World world;
			int x = -10;
			int y = 1;

			// Act
			world = CreateWorld();

			// Assert
			Assert.Null(world.GetStepByCords(x, y));
		}

		[Fact]
		public void World_Get_Possible_Steps() {
			// Arrange
			World world = CreateWorld();

			// Reflection magic (Makes it possible to test private functions
			Type type = typeof(World);
			MethodInfo methodInfo = type.GetMethod("GetPossibleSteps", BindingFlags.Instance | BindingFlags.NonPublic);

			// Act
			Dictionary<string, Step> result = (methodInfo.Invoke(world, null) as Dictionary<string, Step>);

			// Assert
			Assert.Equal(4, result.Count);
		}

		[Fact]
		public void World_Get_Possible_Steps_Descriptions() {
			// Arrange
			World world = CreateWorld();

			// Reflection magic (Makes it possible to test private functions
			Type type = typeof(World);
			MethodInfo methodInfo = type.GetMethod("GetDirectionTexts", BindingFlags.Instance | BindingFlags.NonPublic);

			// Act
			Dictionary<string, Step> possibleDirections = new Dictionary<string, Step>();
			possibleDirections.Add("test", new Step());
			// Second part Reflection magic
			string[] result = methodInfo.Invoke(world, new[] {possibleDirections}) as string[];

			// Assert
			Assert.Equal(1, result.Length);
		}

		[Fact]
		public void World_Print_PossibleMoves() {
			// Arrange
			World world = CreateWorld();

			Player player = new Player("Name", new Gender("Male", "Sir"), null);

			MockIOM iom = new MockIOM();

			// Act
			world.ShowPosibleMovement(iom, player);

			// Assert
			Assert.Equal(2, iom.PrintDescription_Calls.Count);
			Assert.Contains("links ziet u een midden", iom.PrintDescription_Calls[1]);
		}

		[Fact]
		public void World_WhereTo() {
			// Arrange
			World world = CreateWorld();
			Player player = new Player("Name", new Gender("Male", "Sir"), null);
			MockIOM iom = new MockIOM();
			Dictionary<string, Step> possibleDirections = new Dictionary<string, Step>();
			Step step = new Step();
			step.Title = "midden";
			possibleDirections.Add("links", step);

			//Reflection magic
			Type type = typeof(World);

			MethodInfo methodInfo = type.GetMethod("WhereTo", BindingFlags.Instance | BindingFlags.NonPublic);

			// Act
			iom.InputReturns.Enqueue("kip");
			iom.InputReturns.Enqueue("links");
			// Second part Reflection magic
			Step nextStep = methodInfo.Invoke(world, new object[] {iom, possibleDirections, player}) as Step;

			// Assert
			Assert.Contains("geen richting", iom.Print_Calls[0]);
			Assert.Equal("midden", nextStep.Title);
		}

		[Fact]
		public void World_HandleEvent() {
			// Arrange
			World world = CreateWorld();
			Player player = new Player("Name", new Gender("Male", "Sir"), new Bowman(WeaponFactory.Instance));
			MockIOM iom = new MockIOM();
			Dictionary<string, Step> possibleDirections = new Dictionary<string, Step>();
			Step step = new Step();
			step.ThisEvent = new EventBuilder().BuildRandomEvent();
			step.Title = "midden";
			possibleDirections.Add("links", step);

			// Act
			iom.InputReturns.Enqueue(" ");
			iom.InputReturns.Enqueue(" ");

			world.HandleEvent(iom, player, step);

			// Assert
			Assert.Contains("Schiet pijl af", iom.Print_Calls[0]);
			//Assert.Equal("midden", nextStep.Title);
		}

		[Fact]
		public void World_HandleEvent_Character_Wont_Survive() {
			// Arrange
			World world = CreateWorld();
			Player player = new Player("Name", new Gender("Male", "Sir"), new Bowman(WeaponFactory.Instance));
			player.GetCharacter().DamageTaken(24);
			MockIOM iom = new MockIOM();
			Dictionary<string, Step> possibleDirections = new Dictionary<string, Step>();
			Step step = new Step();
			step.ThisEvent = new EventBuilder().BuildRandomEvent();
			step.Title = "midden";
			possibleDirections.Add("links", step);

			// Act
			iom.InputReturns.Enqueue(" ");

			world.HandleEvent(iom, player, step);

			// Assert
			Assert.Contains("Helaas heeft u het niet overleeft!", iom.InputReturns_Calls[2]);
			//Assert.Equal("midden", nextStep.Title);
		}

		[Fact]
		public void World_MoveToNextStep() {
			// Arrange
			World world = CreateWorld();
			Player player = new Player("Name", new Gender("Male", "Sir"), new Bowman(WeaponFactory.Instance));
			MockIOM iom = new MockIOM();
			Dictionary<string, Step> possibleDirections = new Dictionary<string, Step>();
			Step step = new Step();
			step.ThisEvent = new EventBuilder().BuildRandomEvent();
			step.Title = "midden";
			possibleDirections.Add("links", step);

			// Act
			iom.InputReturns.Enqueue(" ");
			iom.InputReturns.Enqueue(" ");

			//Reflection magic
			Type type = typeof(World);
			MethodInfo methodInfo = type.GetMethod("MoveToNextStep", BindingFlags.Instance | BindingFlags.NonPublic);
			methodInfo.Invoke(world, new object[] {step, iom, player});


			world.HandleEvent(iom, player, step);

			// Assert
			Assert.Contains("er staat ineens", iom.PrintDescription_Calls[0]);
		}

		[Fact]
		public void World_MoveToNextStep_Character_No_Health() {
			// Arrange
			World world = CreateWorld();
			Player player = new Player("Name", new Gender("Male", "Sir"), new Bowman(WeaponFactory.Instance));
			MockIOM iom = new MockIOM();
			Step step = new Step();

			// Act
			player.GetCharacter().DamageTaken(100000);

			//Reflection magic
			Type type = typeof(World);
			MethodInfo methodInfo = type.GetMethod("MoveToNextStep", BindingFlags.Instance | BindingFlags.NonPublic);
			methodInfo.Invoke(world, new object[] {step, iom, player});

			world.HandleEvent(iom, player, step);

			// Assert
			Assert.True(iom.PrintDescription_Calls.Count == 0);
		}
	}
}