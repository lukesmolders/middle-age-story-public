using MiddleAgeStory.Test.MockImplementations;
using Xunit;

namespace MiddleAgeStory.Test {
	public class mockIOMTests {
		[Fact]
		public void MockIOMTests_PrintWelcome() {
			// Arrange 
			MockIOM mockIom = new MockIOM();
			string test = "test";

			// Act
			mockIom.PrintWelcome(test);

			// Assert
			Assert.Equal(test, mockIom.PrintWelcome_Calls[0]);
		}

		[Fact]
		public void MockIOMTests_ClearConsole() {
			// Arrange 
			MockIOM mockIom = new MockIOM();

			// Act
			mockIom.ClearConsole();

			// Assert
			Assert.Equal(1, mockIom.ClearConsole_Counter);
		}
	}
}