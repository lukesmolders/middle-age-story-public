﻿using middle_age_story.Enemies;
using middle_age_story.Events;
using middle_age_story.Items;
using Xunit;

namespace MiddleAgeStory.Test {
	public class BuilderTests {
		[Fact]
		public void BuilderTests_Director_GetEnemy() {
			// Arrange
			EnemyBuilder enemyBuilder = new EnemyBuilder();
			Director director = new Director();

			// Act
			Enemy enemy = director.ConstructRandomEnemy(enemyBuilder);

			// Assert
			Assert.Equal(typeof(Enemy), enemy.GetType());
		}

		[Fact]
		public void BuilderTests_Director_GetItem() {
			// Arrange
			ItemBuilder itemBuilder = new ItemBuilder();
			Director director = new Director();

			// Act
			Item item = director.ConstructRandomItem(itemBuilder);

			// Assert
			Assert.Equal(typeof(Item), item.GetType());
		}
	}
}