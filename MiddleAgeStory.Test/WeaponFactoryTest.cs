﻿using middle_age_story.Weapons;
using MiddleAgeStory.Test.MockImplementations;
using Xunit;

namespace MiddleAgeStory.Test {
	public class WeaponFactoryTest {
		/// <summary>
		/// function for delegate
		/// </summary>
		/// <returns></returns>
		public Weapon MakeFake() {
			return new MockWeapon();
		}

		[Fact]
		public void WeaponFactory_Register_New_Weapon() {
			// Arrange
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string weaponName = "Walter p7";

			// Act
			weaponFactory.RegisterNewWeapon(weaponName, MakeFake);

			// Assert
			Assert.IsType<MockWeapon>(MakeFake());
			Assert.Equal(weaponName, weaponFactory.SearchForKnownWeapons(weaponName));
		}

		[Fact]
		public void WeaponFactory_Register_New_Weapon_Allready_Created() {
			// Arrange
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string weaponName = "Walter p7";
			bool expected = false;

			// Act
			weaponFactory.RegisterNewWeapon(weaponName, MakeFake);

			// Assert
			Assert.Equal(expected, weaponFactory.RegisterNewWeapon(weaponName, MakeFake));
		}

		[Fact]
		public void WeaponFactory_Register_New_Weapon_Empty_WeaponName() {
			// Arrange
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string weaponName = null;
			bool expected = false;

			// Act

			// Assert
			Assert.Equal(expected, weaponFactory.RegisterNewWeapon(weaponName, MakeFake));
		}

		[Fact]
		public void WeaponFactory_Create() {
			// Arrange
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string weaponName = "sword";
			Weapon weapon = null;


			// Act
			weapon = weaponFactory.Create(weaponName);

			// Assert
			Assert.Equal(typeof(Sword), weapon.GetType());
		}

		[Fact]
		public void WeaponFactory_Create_Unknown_Weapon() {
			// Arrange
			WeaponFactory weaponFactory = WeaponFactory.Instance;
			string weaponName = "walter p7";
			Weapon weapon = null;

			// Act
			weapon = weaponFactory.Create(weaponName);

			// Assert
			Assert.Null(weapon);
		}
	}
}