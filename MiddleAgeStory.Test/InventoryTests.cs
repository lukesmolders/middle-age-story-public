using middle_age_story.Characters;
using middle_age_story.Items;
using middle_age_story.Weapons;
using MiddleAgeStory.Test.MockImplementations;
using Xunit;

namespace MiddleAgeStory.Test {
	public class InventoryTests {
		[Fact]
		public void InventoryTests_UseInventory() {
			// Arrange
			Item item = new Item(ItemType.apple, 20);
			Gender gender = new Gender("panda", "lief beestje");
			Bowman bowman = new Bowman(WeaponFactory.Instance);
			Player player = new Player("panda", gender, bowman);
			Inventory inventory = new Inventory(player);
			MockIOM mockIom = new MockIOM();
			int startingHealth = player.GetCharacter().Health;

			// Act
			mockIom.InputReturns.Enqueue("1");
			mockIom.InputReturns.Enqueue(" ");
			player.AddItem(item);
			inventory.UseInventory(mockIom);

			// Assert
			Assert.True(startingHealth < player.GetCharacter().Health);
		}
	}
}