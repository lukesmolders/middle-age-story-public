using middle_age_story.Characters;
using middle_age_story.Enemies;
using MiddleAgeStory.Test.MockImplementations;
using Xunit;

namespace MiddleAgeStory.Test {
	public class EnemyTests {
		[Fact]
		public void EnemyTests_Exhausted() {
			// Arrange
			Enemy enemy = new Enemy("Ork");
			enemy.SetAttackDamage(5000);
			CharacterFactory characterFactory;
			MockIOM mockIom = new MockIOM();

			// Act
			mockIom.InputReturns.Enqueue("bowman");
			characterFactory = CharacterFactory.Instance;
			Character actual = CharacterPicker.CreaterCharacterFromPlayerInput(mockIom, characterFactory);
			enemy.AttackAware();

			int i = 0;
			while (i < 5) {
				enemy.Attack(actual, mockIom);
				actual.AttackMove(mockIom, enemy);
				i++;
			}

			// Assert
			Assert.True(actual.Health < 0);
		}

		[Fact]
		public void EnemyTests_Unaware() {
			// Arrange
			Enemy enemy = new Enemy("Ork");
			enemy.SetAttackDamage(1);
			CharacterFactory characterFactory;
			MockIOM mockIom = new MockIOM();
			int health;

			// Act
			mockIom.InputReturns.Enqueue("bowman");
			characterFactory = CharacterFactory.Instance;
			Character actual = CharacterPicker.CreaterCharacterFromPlayerInput(mockIom, characterFactory);
			health = actual.Health;

			enemy.Attack(actual, mockIom);

			// Assert
			Assert.Equal(health, actual.Health);
		}
	}
}